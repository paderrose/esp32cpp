#include <stdio.h>
#include <errno.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "esp_err.h"
#include "nvs_flash.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "sdkconfig.h"

#include "esp_attr.h"
#include "esp_partition.h"
#include <sys/time.h>

#include "esp32cpp/Application.hpp"

using namespace esp32;

portMUX_TYPE microsMux = portMUX_INITIALIZER_UNLOCKED;

unsigned long IRAM_ATTR global_micros() {
    static unsigned long lccount = 0;
    static unsigned long overflow = 0;
    unsigned long ccount;
    portENTER_CRITICAL_ISR(&microsMux);
    __asm__ __volatile__ ( "rsr %0, ccount" : "=a" (ccount) );
    if(ccount < lccount){
        overflow += UINT32_MAX / CONFIG_ESP32_DEFAULT_CPU_FREQ_MHZ;
    }
    lccount = ccount;
    portEXIT_CRITICAL_ISR(&microsMux);
    return overflow + (ccount / CONFIG_ESP32_DEFAULT_CPU_FREQ_MHZ);
}

void Application::main() {
    printf("Thank you for using Esp32cpp version (%s)\n", "0.2.4-100");
    for(;;) {
        micros();
        if(!loop()) break;
    }
}
void Application::delay(int ms) {
  vTaskDelay(ms / portTICK_PERIOD_MS);
}
unsigned long Application::micros() {
  return global_micros();
}
unsigned long Application::millis() {
  return xTaskGetTickCount() * portTICK_PERIOD_MS;
}
Application* Application::m_pInstance = 0;

extern "C" void app_main()
{
    nvs_flash_init();
    if(Application::Current().setup())
      Application::Current().Create(1);
}
