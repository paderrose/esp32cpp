#ifndef PROPERTY_HPP
#define PROPERTY_HPP

#include "auto_ptr.hpp"
#include <stdint.h>

namespace esp32 {
    template <typename T, class TBase>
    class property {
    public:
        using setter_type = void (TBase::*)(const T);
        using getter_type = T (TBase::*)();

        property(auto_ptr<TBase> base, getter_type gtyp, setter_type styp ) {
            m_aupClass = base;
            m_clGetter = gtyp;
            m_cSetter = styp;
        }
        operator T() const {
            assert(m_clGetter != 0);
            assert(m_aupClass.get() != 0);
            return (m_aupClass->*m_clGetter());
        }
        const auto_ptr<TBase> operator = (const T& value) {
            (m_aupClass->*m_cSetter(value));
            return m_aupClass;
        }
        const TBase& operator = (const property<T,TBase>& other) {
            m_aupClass = other.m_aupClass;
            m_clGetter = other.m_cSetter;
            m_cSetter = other.m_cSetter;
        }
    private:
        auto_ptr<TBase> m_aupClass;
        getter_type m_clGetter;
        setter_type m_cSetter;
    };
}

#ifndef prop_t
#define prop_t(base, type, name, get, set) \
         std::property<(type), (base)> (name){std::auto_ptr<base>((this)), (get), (set)}

#define prop8_t(base, name, get, set) prop_t(base, int8_t, (name), (get), (set))
#define prop16_t(base, name, get, set) prop_t(base, int16_t, (name), (get), (set))
#define prop32_t(base, name, get, set) prop_t(base, int32_t, (name), (get), (set))
#define prop64_t(base, name, get, set) prop_t(base, int64_t, (name), (get), (set))

#define propu8_t(base, name, get, set) prop_t(base, uint8_t, (name), (get), (set))
#define propu16_t(base, name, get, set) prop_t(base, uint16_t, (name), (get), (set))
#define propu32_t(base, name, get, set) prop_t(base, uint32_t, (name), (get), (set))
#define propu64_t(base, name, get, set) prop_t(base, uint64_t, (name), (get), (set))

#define propl_t(base, name, get, set) prop_t(base, long, (name), (get), (set))
#define propf_t(base, name, get, set) prop_t(base, float, (name), (get), (set))
#define propd_t(base, name, get, set) prop_t(base, double, (name), (get), (set))
#define propc_t(base, name, get, set) prop_t(base, char, (name), (get), (set))

#define propul_t(base, name, get, set) prop_t(base,unsigned long, (name), (get), (set))
#define propf_t(base, name, get, set) prop_t(base, float, (name), (get), (set))
#define propd_t(base, name, get, set) prop_t(base, double, (name), (get), (set))
#define propuc_t(base, name, get, set) prop_t(base,unsigned char, (name), (get), (set))

#define propcp_t(base, name, get, set) prop_t(base,const char*, (name), (get), (set))


#endif

#endif
