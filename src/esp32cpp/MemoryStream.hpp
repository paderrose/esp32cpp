#ifndef ESP32_MEMORYSTREAM_HPP
#define ESP32_MEMORYSTREAM_HPP

#include "Stream.hpp"

namespace esp32 {
	class MemoryStream : public Stream {
	public:
		MemoryStream(size_t len);
		MemoryStream(void* buffer, size_t len) : MemoryStream(buffer, 0, len, true) { }
		MemoryStream(void* buffer, int index, int count) : MemoryStream(buffer, index, count, true) { }
		MemoryStream(void* buffer, int index, int count, bool writable);
		~MemoryStream();

		virtual void 			write(uint8_t pData);
		virtual uint8_t 	read();

		virtual uint32_t 	read(void* pData, uint32_t offset, size_t len);
		virtual uint32_t 	write(void* pData, uint32_t offset, size_t len);

		virtual bool		  setSize(long size);
		virtual long 			seek(long lPos, SeekOrigin eOrigin);
	private:
		bool resizeBuffer(long size);
	private:
		char* m_pBuffer;
		uint32_t m_iCapacity;
		uint32_t m_lPosition;
		bool m_bIsOpen;
	};
};
#endif
