
#ifndef RAY_HPP
#define RAY_HPP

#include "vector3.hpp"

namespace esp32
{
    namespace math
    {
        template<typename T>
	      class ray
        {
        public:
          typedef ray<T> self_type;
          typedef T value_type;
          typedef T* pointer;

          ray (vector3<value_type> position, vector3<value_type> direction) {
            m_position = position;
            m_directon = direction;
          }
          ray(const ray<value_type>& other) {
            m_position = other.m_position;
            m_directon = other.m_directon;
          }
          const vector3<value_type>& position() { return m_position; }
          void position(const vector3<value_type>& p) { m_position = p; }

          const vector3<value_type>& direction() { return m_directon; }
          void direction(const vector3<value_type>& p) { m_directon = p; }
        private:
          vector3<T> m_position;
          vector3<T> m_directon;
        };
        template <typename T>
        inline bool operator == (ray<T> a, ray<T> b) {
            return a.direction() == b.direction() &&
                    a.position() == b.position();
        }
        template <typename T>
        inline bool operator != (ray<T> a, ray<T> b) {
            return a.direction() != b.direction() &&
                    a.position() != b.position();
        }
    }
}

#endif /* BOUNDINGSPHERE_HPP */
