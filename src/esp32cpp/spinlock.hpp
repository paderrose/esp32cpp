
#ifndef _STL_ESP32_SPINLOK_HPP_
#define _STL_ESP32_SPINLOK_HPP_

#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/task.h"

namespace esp32
{
  class spinlock {
  public:
    using native_handle_type = SemaphoreHandle_t;

    spinlock(int count = 10);
    ~spinlock();

    void lock();
    void unlock();
    bool try_lock();

    int getCount();

    native_handle_type native_handle();
  private:
    native_handle_type _m_locked;
  };
}

 #endif
