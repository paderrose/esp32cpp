#ifndef ESP32_RINGBUFFERSTREAM_HPP
#define ESP32_RINGBUFFERSTREAM_HPP

#include <freertos/FreeRTOS.h>
#include <freertos/queue.h>
#include <freertos/ringbuf.h>


#include "Stream.hpp"

namespace esp32 {
	class ringbufferStream : public Stream {
	public:
		ringbufferStream(const int size);
	  ~ringbufferStream();

		virtual uint8_t 	read();
		virtual uint32_t 	read(void* pData, uint32_t offset, size_t len);

		virtual void 			write(uint8_t pData);
		virtual uint32_t 	write(void* pData, uint32_t offset, size_t len);

		virtual bool		 setSize(long size) { return false; }
		long 		 				 seek(long lPos, SeekOrigin eOrigin) { return 0; }
		virtual void 		 flush();

		uint32_t				 getUsedBytes();
		uint32_t				 getFreeBytes();

		bool						 isEmpty() { return getUsedBytes() == 0; }
		bool						 isFull()  { return getUsedBytes() == getSize(); }
	private:
		volatile uint32_t  m_uiCount;	/**< Number of bytes currently stored in the buffer. */
		volatile char *    m_puiIn;		/**< Current storage location in the circular buffer. */
		volatile char *    m_puiOut;		/**< Current retrieval location in the circular buffer. */
		char* 					   m_puiStart;				/**< Pointer to the start of the buffer's underlying storage array. */
		char* 					 	 m_puiEnd;				/**< Pointer to the end of the buffer's underlying storage array. */
		char*							 m_pBuffer;
	};
}

#endif
