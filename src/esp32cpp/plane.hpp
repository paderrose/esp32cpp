

#ifndef PLANE_HPP
#define PLANE_HPP

#include "vector4.hpp"

namespace esp32
{
    namespace math
    {
        template < typename T > class plane;
        typedef plane<float>  planef;
        typedef plane<double> planed;
        typedef plane<int>    planei;
        typedef plane<short>  planes;

        template <typename T>
        class plane : public vector4<T>
        {
        public:
            typedef T value_type;
            typedef plane<T> self_type;
            typedef T* pointer;

            plane(void)
                : vector4<T>()							{}
	    plane(const value_type _x, const value_type _y, const value_type _z, const value_type _w)
                : vector4<T>(_x, _y, _z, _w)					{}
            plane(const value_type f)
                :  vector4<T>(f, f, f, f)								{}
	    plane(const self_type& vec)
                : vector4<T>(vec.x, vec.y, vec.z, vec.w)			{}
	    plane(const vector3<T>& vec)
                : vector4<T>(vec)			{}
            plane(const vector2<T>& vec)
                : vector4<T>(vec)			{}
            plane(const value_type *lpf)
                : vector4<T>(lpf)		{}

        };

        template <typename T>
        inline plane<T>	plane_frompointnormal(const vector3<T>& p, const vector3<T>& n)
            {return plane<T>(n, -n.x * p.x - n.y * p.y - n.z * p.z);}
	template <typename T>
        inline plane<T>	plane_frompoints(const vector3<T>& v1, const vector3<T>& v2, const vector3<T>& v3)
            {return plane_frompointnormal(v1, cross(v3 - v2, v1 - v2));}

    }
}

#endif /* PLANE_HPP */
