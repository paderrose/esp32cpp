#ifndef _STL_ESP32_APPLICATION_HPP_
#define _STL_ESP32_APPLICATION_HPP_

#include "task.hpp"
#include "sdkconfig.h"

namespace esp32 {
  class Application : public Task {
  private:
    Application() : Task("ApplicationTask", 1, 8192) { }
  public:
    void delay(int ms);
    unsigned long millis();
    unsigned long micros();

    static Application& Current() {
      if(m_pInstance == 0) m_pInstance = new Application();
       return *m_pInstance;
     }
     bool setup();
     bool loop();
  protected:
    void main();
  private:
    static Application* m_pInstance;
  };
}
#endif
