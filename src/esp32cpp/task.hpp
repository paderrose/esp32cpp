
/*
 * File:   task.hpp
 * Author: annas
 *
 * Created on 17. Januar 2018, 12:58
 */

#ifndef _STL_ESP32_TASK_HPP_
#define _STL_ESP32_TASK_HPP_

#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/task.h"
#include "mutex.hpp"
#include "unknown.hpp"

namespace esp32
{
  class Task : public object {
  public:
    Task(char const* strName, unsigned int uiPriority,
         unsigned short  usStackDepth = configMINIMAL_STACK_SIZE);
    Task(xTaskHandle xhandle) { handle = xhandle; }
    ~Task();

    void Create();
    void Create(int uiCore);
    void Delete();

    const char* getName() { return m_strName; }
    unsigned int getPriority() { return uxTaskPriorityGet(handle);}
    unsigned short getStackDepth() { return m_usStackDepth;}
    xTaskHandle getHandle() { return handle; }

    void setPriority(unsigned int uiPriority) { vTaskPrioritySet(handle, uiPriority); }

    uint32_t getTimeSinceStart() {
      return (uint32_t)(xTaskGetTickCount()*portTICK_PERIOD_MS);
    }
    void suspend() { vTaskSuspend( handle ); }
    void resume() { vTaskResume(handle); }

    static void suspend(Task *t) { vTaskSuspend((t == 0) ? NULL : t->handle); }
    static void resume(Task *t) { vTaskResume(t->handle); }

    static void yield() { taskYIELD(); }
    void lock() { m_mutex.lock(); }
    void unlock() { m_mutex.unlock(); }
  protected:
    virtual void main() { }
    static void runtaskstub(void* parm);
  protected:
    xTaskHandle handle;
    const char* m_strName;
    unsigned int m_uiPriority;
    unsigned short m_usStackDepth;
    mutex m_mutex;
  };
}

 #endif
