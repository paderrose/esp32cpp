#ifndef _STL_ESP32_DMABuffer_HPP_
#define _STL_ESP32_DMABuffer_HPP_

#include "unknown.hpp"

extern "C" {
	#include <soc/i2s_reg.h>
	#include <soc/i2s_struct.h>
}

#include <rom/lldesc.h>

namespace esp32 {

	enum class dma_buffer_owner {
		SW = LLDESC_SW_OWNED,
		HW = LLDESC_HW_OWNED
	};

	class dma_buffer : public object {
	public:
		dma_buffer();
		dma_buffer(uint32_t lenght, uint32_t size, uint32_t offset,
			         dma_buffer_owner owner,
							 bool sosf = true, bool eof = false, bool empty = false);
		~dma_buffer();

		virtual uint32_t   	read(uint8_t* pData, uint32_t length);
		virtual void		    write(uint8_t* pData, uint32_t length);

		lldesc_t*  	getDesc() 			{ return &m_desc; }
		uint32_t   	getLength() 		{ return m_desc.length/4; }
		dma_buffer* getNext() 			{ return m_pNext; }
		bool      	isEOF() 				{ return m_desc.eof != 0; }

		void       setNext(dma_buffer* pNext); // direct next to this object
		void       setLast(dma_buffer* pNext); // if next 0 then setNext else next from next object
	private:
		lldesc_t   m_desc;
		dma_buffer* m_pNext;
	};
}

#endif
