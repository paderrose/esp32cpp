
#ifndef _STL_ESP32_PWM_HPP_
#define _STL_ESP32_PWM_HPP_

#include <driver/ledc.h>

namespace esp32
{
  class esp32pwm {
  public:
    esp32pwm(int gpioNum, uint32_t frequency = 100,
		    ledc_timer_bit_t bitSize = LEDC_TIMER_10_BIT,
		    ledc_timer_t timer       = LEDC_TIMER_0,
		    ledc_channel_t channel   = LEDC_CHANNEL_0);

  	uint32_t getDuty()   {
      return ::ledc_get_duty(LEDC_HIGH_SPEED_MODE, m_channel);
    }
  	uint32_t getFrequency() {
      return ::ledc_get_freq(LEDC_HIGH_SPEED_MODE, m_timer);
    }
  	void     setDuty(uint32_t duty) {
      ESP_ERROR_CHECK(::ledc_set_duty(LEDC_HIGH_SPEED_MODE, m_channel, duty));
	    ESP_ERROR_CHECK(::ledc_update_duty(LEDC_HIGH_SPEED_MODE, m_channel));
    }
  	//void     setDutyPercentage(uint8_t percent);
  	void     setFrequency(uint32_t freq) {
      ESP_ERROR_CHECK(::ledc_set_freq(LEDC_HIGH_SPEED_MODE, m_timer, freq));
    }
  	void     stop(bool idleLevel=false) {
      ESP_ERROR_CHECK(::ledc_stop(LEDC_HIGH_SPEED_MODE, m_channel, idleLevel));
    }
  private:
    ledc_channel_t   m_channel;
  	ledc_timer_t     m_timer;
  	//ledc_timer_bit_t m_dutyResolution; // Bit size of timer.
  };
}

#endif
