#ifndef ESP32_STREAM_HPP
#define ESP32_STREAM_HPP

#include <stdint.h>
#include <stddef.h>

namespace esp32 {
	  enum class SeekOrigin {
			Start,
			Current,
			End
		};
		class Stream  {
		public:
			Stream() { }
			Stream(long size, bool canWrite, bool canRead, bool canSeek)
				: m_lSize(size), m_bCanWrite(canWrite),
				  m_bCanRead(canRead), m_bCanSeek(canSeek) { }

			virtual void 			open() 	{ m_bOpen = true; }
			virtual void 			close() { m_bOpen = false; }

			virtual uint32_t 	read(void* pData, uint32_t offset, size_t len) = 0;//
			virtual uint32_t 	write(void* pData, uint32_t offset, size_t len) = 0;//

			virtual void 			write(uint8_t pData) = 0; //
			virtual uint8_t 	read() = 0; //

			virtual long 		 seek(long lPos, SeekOrigin eOrigin) = 0; //

			virtual long		 getSize() { return m_lSize; }
			virtual bool		 setSize(long size) = 0;

			virtual bool		canWrite() { return m_bOpen & m_bCanWrite; }
			virtual bool 		canRead() { return m_bOpen & m_bCanRead; }
			virtual bool		canSeek() { return m_bOpen & m_bCanSeek; }
			virtual bool 		isOpen()  { return m_bOpen; }

			virtual void 		flush() { }
		protected:
			long m_lSize;
			bool m_bOpen;
			bool m_bCanWrite;
			bool m_bCanRead;
			bool m_bCanSeek;
		};
}

#endif
