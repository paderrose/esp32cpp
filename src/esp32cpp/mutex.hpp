
#ifndef _STL_ESP32_MUTEX_HPP_
#define _STL_ESP32_MUTEX_HPP_

#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/task.h"

namespace esp32
{
  class mutex {
  public:
    using native_handle_type = SemaphoreHandle_t;

    mutex();
    mutex(const mutex& m);
    ~mutex();

    void lock();
    void unlock();
    bool try_lock();

    native_handle_type native_handle();
  private:
    native_handle_type _m_locked;
  };
}

 #endif
