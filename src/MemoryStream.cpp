#include "esp32cpp/MemoryStream.hpp"

#include <stdlib.h>
#include <string.h>

namespace esp32 {
	MemoryStream::MemoryStream(size_t len)
		: Stream(len, true, true, true)  {
			m_pBuffer = (char*)malloc(len * sizeof(char));
			m_iCapacity = len;
			m_lPosition = 0;
	}
	MemoryStream::MemoryStream(void* buffer, int index, int count, bool writable)
		: Stream(index + count, writable, true, true) {
			m_iCapacity = index + count;

			m_pBuffer = (char*)malloc(m_iCapacity * sizeof(char));
			memcpy(m_pBuffer, buffer, index + count);

			m_lPosition = 0;
	}
	MemoryStream::~MemoryStream() {
		delete[] m_pBuffer;
	}
	uint32_t 	MemoryStream::read(void* pData, uint32_t offset, size_t count) {
		if(!isOpen() && !canRead() ) return -1;

		int n = m_lSize - m_lPosition;
		if (n > count) n = count;
		if (n <= 0) return 0;

		//memcpy( destination + pos, source + off, count);
		memcpy( ((char*)pData) + m_lPosition, ((char*)m_pBuffer) + offset, n);
		m_lPosition += n;

		return n;
	}
	uint8_t 	MemoryStream::read() {
		if(!isOpen() && !canRead() ) return -1;
		if (m_lPosition >= m_lSize) return -1;

		return (uint8_t)m_pBuffer[m_lPosition++];
	}
	uint32_t 	MemoryStream::write(void* pData, uint32_t offset, size_t count) {
		if(!isOpen() && !canWrite() ) return -1;

		int i = m_lPosition + count;
		if(i < 0) return -1;

		if (i > m_lSize) {
      if (i > m_iCapacity) {
          resizeBuffer(i);
      }
      m_lSize = i;
  	}
		// Buffer.InternalBlockCopy(buffer, offset, _buffer, _position, count);
		memcpy( ((char*)m_pBuffer) + offset, ((char*)pData) + m_lPosition, count);
		m_lPosition = i;
		return count;
	}
  void 			MemoryStream::write(uint8_t pData) {
		if(!isOpen() && !canWrite() ) return;

		if (m_lPosition >= m_lSize) {
			int newLength = m_lPosition + 1;
			if (newLength > m_lSize) {
	      if (newLength > m_iCapacity) {
	          resizeBuffer(newLength);
	      }
	      m_lSize = newLength;
	  	}
		}
		m_pBuffer[m_lPosition++] = pData;
	}

	long MemoryStream::seek(long offset, SeekOrigin eOrigin) {
		if(!isOpen() && !canSeek() ) return -1;

		switch (eOrigin) {
			case SeekOrigin::Start: m_lPosition = offset; break;
			case SeekOrigin::End: m_lPosition = m_lSize + offset; break;
			case SeekOrigin::Current: m_lPosition += offset; break;
		}
		return m_lPosition;
	}
	bool MemoryStream::setSize(long size) {
		if (size > UINT32_MAX	) return false;
		if(resizeBuffer(size)) {
			m_lSize = size;
      if (m_lPosition > size) m_lPosition = size;
			return true;
		}
		return false;
	}
	bool MemoryStream::resizeBuffer(long size) {
		if(size <= 0) return false;
		if(size > m_iCapacity){
			int newCapacity = size < 256 ? 256 : size;
			newCapacity = (newCapacity < m_iCapacity * 2) ? m_iCapacity * 2 : newCapacity;

			char* temp = new char[newCapacity];
			memset(temp, 0, sizeof(char) * newCapacity);
			memcpy(temp, m_pBuffer, sizeof(char) *  m_iCapacity);
			delete[] m_pBuffer;
			m_pBuffer = temp;

			m_iCapacity = newCapacity;

			return true;
		}
		return false;
	}
}
