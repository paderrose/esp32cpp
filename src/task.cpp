#include "esp32cpp/task.hpp"


namespace esp32 {
  Task::Task(char const* strName, unsigned int uiPriority,
       unsigned short  usStackDepth) {
    m_strName = strName;
    m_uiPriority = uiPriority;
    m_usStackDepth = usStackDepth;

  }
  Task::~Task() { vTaskDelete(handle); }

  void Task::Create() {
    xTaskCreate(&runtaskstub, m_strName,
                ((m_usStackDepth + sizeof(uint32_t) - 1) / sizeof(uint32_t)),
                this, m_uiPriority, &handle);
  }
  void Task::Create(int uiCore) {
    xTaskCreatePinnedToCore(&runtaskstub, m_strName,
               ((m_usStackDepth + sizeof(uint32_t) - 1) / sizeof(uint32_t)),
                this, m_uiPriority, &handle, uiCore);
  }
  void Task::Delete() {
    vTaskDelete(handle);
  }

  void Task::runtaskstub(void* parm) {
    (static_cast<Task*>(parm))->main();
  }
}
