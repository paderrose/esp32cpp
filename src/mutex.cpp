
#include "esp32cpp/mutex.hpp"



namespace esp32
{

    mutex::mutex() {
        _m_locked = xSemaphoreCreateBinary();
    }
    mutex::mutex(const mutex& m) {
      _m_locked = m._m_locked;
    }
    mutex::~mutex()
    {
        unlock();
        vSemaphoreDelete(_m_locked);
    }
    void mutex::lock()
    {
        xSemaphoreTakeRecursive(_m_locked, portMAX_DELAY);
    }
    void mutex::unlock()
    {
      xSemaphoreGiveRecursive(_m_locked);
    }
    bool mutex::try_lock()
    {
       return xSemaphoreTakeRecursive(_m_locked, 0);
    }
    mutex::native_handle_type mutex::native_handle()
    {
        return _m_locked;
    }
}
