#include "esp32cpp/esp32pwm.hpp"

namespace esp32
{
  esp32pwm::esp32pwm(int gpioNum, uint32_t frequency,
      ledc_timer_bit_t bitSize,
      ledc_timer_t timer,
      ledc_channel_t channel) {
        ledc_timer_config_t timer_conf;
      	//timer_conf.duty_resolution    = dutyResolution;
      	timer_conf.freq_hz    = frequency;
      	timer_conf.speed_mode = LEDC_HIGH_SPEED_MODE;
      	timer_conf.timer_num  = timer;
      	ESP_ERROR_CHECK(::ledc_timer_config(&timer_conf));

      	ledc_channel_config_t ledc_conf;
      	ledc_conf.channel    = channel;
      	ledc_conf.duty       = 0;
      	ledc_conf.gpio_num   = gpioNum;
      	ledc_conf.intr_type  = LEDC_INTR_DISABLE;
      	ledc_conf.speed_mode = LEDC_HIGH_SPEED_MODE;
      	ledc_conf.timer_sel  = timer;
      	ESP_ERROR_CHECK(::ledc_channel_config(&ledc_conf));

      	this->m_channel        = channel;
      	this->m_timer          = timer;
      	//this->m_dutyResolution = dutyResolution;
      }
}
