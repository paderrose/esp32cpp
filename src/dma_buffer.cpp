#include "esp32cpp/dma_buffer.hpp"
#include <esp_intr_alloc.h>
#include <esp_log.h>
#include <stdio.h>

namespace esp32 {
	dma_buffer::dma_buffer()
	 	: dma_buffer(0, 2048, 0, dma_buffer_owner::HW)  {

	}
	dma_buffer::dma_buffer(uint32_t length, uint32_t size, uint32_t offset,
						  dma_buffer_owner owner,
						 bool sosf, bool eof, bool empty) {
		m_desc.length = length;//
		m_desc.size   = size;//
		m_desc.owner  = owner == dma_buffer_owner::HW ? LLDESC_HW_OWNED : LLDESC_SW_OWNED; //dma_buffer_owner.HW
		m_desc.sosf   = sosf ? 1 : 0;//
		m_desc.offset = offset; //
		m_desc.eof    = eof ? 1 : 0;
		m_desc.empty  = empty ? 1 : 0;
		m_desc.buf    = new uint8_t[m_desc.size];
		m_pNext       = nullptr;
	}
	dma_buffer::~dma_buffer() {
		if(m_desc.buf)  { delete[] m_desc.buf; m_desc.buf = nullptr; }
	}
	uint32_t  dma_buffer::read(uint8_t* pData, uint32_t length) {
		uint32_t i = 0;
		uint8_t* pBuf = (uint8_t*)m_desc.buf;

		if (length > getLength()) {
			length = getLength();
		}
		for (i=0; i<length; i+=2, pBuf += 4) {
			*pData = pBuf[2]; pData++;
			*pData = pBuf[0]; pData++;
		}
		return i;
	}
	void memcpy (void *dest, const void *src, size_t len)
{

}
	void  dma_buffer::write(uint8_t* pData, uint32_t length) {
		uint8_t* pBuf = (uint8_t*)m_desc.buf;
		//TODO: Dummy
		char *d = (char*)pBuf;
	  const char *s = (char*)pData;
	  while (length--)
	    *d++ = *s++;
	}
	void dma_buffer::setNext(dma_buffer* pNext) {
		m_pNext = pNext;
		m_desc.empty = 0;
		m_desc.qe.stqe_next = pNext->getDesc();
	}
	void dma_buffer::setLast(dma_buffer* pNext) {
		if(m_pNext) m_pNext->setLast(pNext);
		else setNext(pNext);
	}
}
