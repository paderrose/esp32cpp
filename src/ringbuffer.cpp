#include "esp32cpp/ringbuffer.hpp"
#include <stdio.h>
#include <string.h>

namespace esp32 {
	ringbufferStream::ringbufferStream(const int size)
			: Stream(size, true, true, false) {
		m_pBuffer = new char[size];
		m_uiCount = 0;
		m_puiIn = m_pBuffer;
		m_puiOut = m_pBuffer;
		m_puiStart = &m_pBuffer[0];
		m_puiEnd = &m_pBuffer[size];
	}
	void ringbufferStream::flush() {
		m_uiCount = 0;
		m_puiIn = m_puiStart;
		m_puiOut = m_puiStart;
	}
	 void ringbufferStream::write(uint8_t pData) {
		 *m_puiIn = pData;
		 if (++m_puiIn == m_puiEnd)
	  		m_puiIn = m_puiStart;
		//lock
		m_uiCount++;
		//unlock
	}
	 uint8_t ringbufferStream::read() {
		uint8_t data = *m_puiOut;
		if (++m_puiOut == m_puiEnd)
			m_puiOut = m_puiStart;
			//lock
			m_uiCount--;
			//unlock
		return data;
	}
	uint32_t	ringbufferStream::getUsedBytes()  {
		uint32_t count;
		//lock
		count = m_uiCount;
		//unlock
		return count;
	}
	uint32_t	ringbufferStream::getFreeBytes() {
		return (uint32_t)(m_lSize - getUsedBytes());
	}

	 uint32_t ringbufferStream::read(void* pData, uint32_t offset, size_t len) {
		uint32_t readSize = 0;
		char* _tmpBuf = new char[len+1];

		for(readSize = 0; readSize < len; readSize++) {
			_tmpBuf[readSize] = read();
		}
		memcpy(pData, _tmpBuf, readSize);
		delete[] _tmpBuf;

		return readSize;
	}
	uint32_t ringbufferStream::write(void* pData, uint32_t offset, size_t len) {
		uint32_t writeSize = 0;
		char* _tmpBuf = new char[len+1];

		memcpy(_tmpBuf, pData, len);

		for(writeSize = 0; writeSize < len; len++) {
			write(_tmpBuf[writeSize]);
		}
		delete[] _tmpBuf;
		
		return writeSize;
	}

}
