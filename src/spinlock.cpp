
#include "esp32cpp/spinlock.hpp"



namespace esp32
{

    spinlock::spinlock(int count)
    {
        _m_locked = xSemaphoreCreateCounting(0x7fffffff, count);
    }
    spinlock::~spinlock()
    {
        unlock();
        vSemaphoreDelete(_m_locked);
    }
    void spinlock::lock()
    {
        xSemaphoreTake(_m_locked, portMAX_DELAY);
    }
    void spinlock::unlock()
    {
      xSemaphoreGive(_m_locked);
    }
    bool spinlock::try_lock()
    {
       return xSemaphoreTakeRecursive(_m_locked, 0);
    }
    spinlock::native_handle_type spinlock::native_handle()
    {
        return _m_locked;
    }
    int spinlock::getCount() {
       return static_cast<int>(uxQueueMessagesWaiting(_m_locked));
    }
}
